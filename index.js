const vCard = require('vcf')
const fs = require('fs')
const path = require('path')

let str = fs.readFileSync('kontakty.vcf', 'utf8')
const replacements = [
    [/=C3=A1/g, 'á'],
    [/=C4=8D/g, 'č'],
    [/=C5=99/g, 'ř'],
    [/=C4=9B/g, 'ě'],
    [/=C4=8C/g, 'Č'],
    [/=C3=AD/g, 'í'],
    [/=C3=BD/g, 'ý'],
    [/=C5=A1/g, 'š'],
    [/=C5=A0/g, 'Š'],
    [/=C5=BE/g, 'ž'],
    [/=C3=A9/g, 'é'],
    [/=C5=BD/g, 'Ž'],
    [/=C5=88/g, 'ň'],
    [/=C5=91/g, 'ö'],
    [/=C3=BA/g, 'ú'],
    [/=C5=AF/g, 'ů'],
    [/=C4=8F/g, 'ď'],
    [/=C3=81/g, 'Á'],
    [/=C5=A5/g, 'ť'],
    [/=C5=98/g, 'Ř'],
    [/=C3=9A/g, 'Ú'],
    [/=C3=89/g, 'Ě'],
    [/=C3=BC/g, 'ü'],
    [/=C5=87/g, 'Ň'],
]
for(const [k, v] of replacements) {
    str = str.replace(k, v)
}
fs.writeFileSync('out.vcf', str)
str.split('\r\n\r\n').forEach((card, i) => {
    fs.writeFileSync(path.join('out',i+'.vcf'), card+'\r\n', 'utf-8')
})
